/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.promotionenginedemo.service;

public interface PromotionenginedemoService
{
	String getHybrisLogoUrl(String logoCode);

	void createLogo(String logoCode);
}
