/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.promotionenginedemo.setup;

import static de.hybris.promotionenginedemo.constants.PromotionenginedemoConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import de.hybris.promotionenginedemo.constants.PromotionenginedemoConstants;
import de.hybris.promotionenginedemo.service.PromotionenginedemoService;


@SystemSetup(extension = PromotionenginedemoConstants.EXTENSIONNAME)
public class PromotionenginedemoSystemSetup
{
	private final PromotionenginedemoService promotionenginedemoService;

	public PromotionenginedemoSystemSetup(final PromotionenginedemoService promotionenginedemoService)
	{
		this.promotionenginedemoService = promotionenginedemoService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		promotionenginedemoService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return PromotionenginedemoSystemSetup.class.getResourceAsStream("/promotionenginedemo/sap-hybris-platform.png");
	}
}
