/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.promotionenginedemo.controller;

import static de.hybris.promotionenginedemo.constants.PromotionenginedemoConstants.PLATFORM_LOGO_CODE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import de.hybris.promotionenginedemo.service.PromotionenginedemoService;


@Controller
public class PromotionenginedemoHelloController
{
	@Autowired
	private PromotionenginedemoService promotionenginedemoService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String printWelcome(final ModelMap model)
	{
		model.addAttribute("logoUrl", promotionenginedemoService.getHybrisLogoUrl(PLATFORM_LOGO_CODE));
		return "welcome";
	}
}
