/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Apr 6, 2020, 9:37:13 PM                     ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.promotionenginedemo.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast"})
public class GeneratedPromotionenginedemoConstants
{
	public static final String EXTENSIONNAME = "promotionenginedemo";
	public static class TC
	{
		public static final String RULEBASEDADDUSERTOUSERGROUPACTION = "RuleBasedAddUserToUserGroupAction".intern();
	}
	public static class Attributes
	{
		// no constants defined.
	}
	
	protected GeneratedPromotionenginedemoConstants()
	{
		// private constructor
	}
	
	
}
