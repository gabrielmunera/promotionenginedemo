/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.promotionenginedemo.constants;

/**
 * Global class for all Promotionenginedemo constants. You can add global constants for your extension into this class.
 */
public final class PromotionenginedemoConstants extends GeneratedPromotionenginedemoConstants
{
	public static final String EXTENSIONNAME = "promotionenginedemo";

	private PromotionenginedemoConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "promotionenginedemoPlatformLogo";
}
